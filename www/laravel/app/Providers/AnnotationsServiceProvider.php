<?php
  namespace App\Providers;
  use Collective\Annotations\AnnotationsServiceProvider as ServiceProvider;
  class AnnotationsServiceProvider extends ServiceProvider {
      protected $scanWhenLocal = true;
      protected $scanControllers = true;
  }