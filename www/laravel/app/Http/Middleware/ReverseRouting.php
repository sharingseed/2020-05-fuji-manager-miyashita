<?php
  namespace App\Http\Middleware;
  use Illuminate\Routing\Route;
  use Illuminate\Http\Request;
  use Illuminate\Http\Response;
  use Closure;
  class ReverseRouting
  {
      public function handle(Request $request, Closure $next)
      {
          $ext = "html";
          $engine = "blade";
          /** @var Response */
          $response = $next($request);
          if ($request->expectsJson()) {
          	return $response;
          }
          /** @var Route */
          $route = $request->route();
          // RoutingがなければリクエストURLからファイルを探す
          $url = isset($route)
          	? preg_replace('!\{[^\}]+\?\}!', '', $route->uri())
          	: $request->server->get("REQUEST_URI");
          // URLからテンプレートファイルを探す
          $file = preg_replace('!/+$!', '', public_path($url));
          if (!\Str::endsWith($file, "." . $ext)) {
              $file = file_exists($found = $file . "." . $ext)
              	? $found
              	: $file . "/index." . $ext;
          }
          // テンプレートファイルがあれば処理
          if (is_file($file)) {
              // 応答のJSONをテンプレートにアサインする
              $vars = is_array($response) ? $response : [];
              if ($response) {
                  if ($response->getStatusCode() !== 200
                  	|| $response->headers->get('Content-Type') !== 'application/json') {
                  	return $response;
                  }
                  $content = $response->getContent();
                  $vars = json_decode($content, true);
              }
              \View::addLocation(public_path("_include"));
              \View::addExtension($ext, $engine);
              error_reporting(error_reporting() ^ E_NOTICE);
              $response = response(\View::file($file, $vars));
          }
          return $response;
      }
  }