module.exports = {
  commitDeployConfig: { appRootDir: "../", name: "dev-setup" },
  devDir: "dev",
  defaultSource: "../../2019-11-libs/generative/sample",
  includes: [
    { name: "bin/legacy" },
    // { name: "templates/project-root" },
    // { name: "templates/laravel-model" },
    // { name: "templates/nuxt-crud" },
    // { name: "templates/20XX-XX-some-template",
    //   alias: "templates/some-template",
    //   source: "../../20XX-XX-some-project/dev" },
  ]
}
