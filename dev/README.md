## Usage
```
# In devDir
cd dev

# Deploy config files
generative init

# Deploy and update sources
generative update

# ... Read README in sources
```
