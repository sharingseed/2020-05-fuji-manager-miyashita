import { Middleware } from '@nuxt/types'
import { initializeStore } from "../store"
      
let initialized = false
const middleware: Middleware = async (app) => {
  if ( ! initialized) {
    initialized = true
    await initializeStore(app) 
  }
}
export default middleware